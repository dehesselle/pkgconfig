# pkg-config for macOS

Build [`pkg-config`](https://www.freedesktop.org/wiki/Software/pkg-config/) without any package managers.

## License

[GPL-2.0-or-later](LICENSE)

